package controllers

import (
	"context"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"go-mux-api/models"
	"go-mux-api/responses"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"time"
)

func CreateBook() http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		params := mux.Vars(r)
		shopId := params["shopId"]

		var shop models.Shop
		var book models.Book

		defer cancel()

		objId, _ := primitive.ObjectIDFromHex(shopId)

		err := shopCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&shop)
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		// Parse request body of book
		json.NewDecoder(r.Body).Decode(&book)

		newBook := models.Book{
			Id:     uuid.New().String(),
			Title:  book.Title,
			Author: book.Author,
		}

		// append new book to book array
		shop.BookList = append(shop.BookList, newBook)
		update := bson.M{"name": shop.Name, "location": shop.Location, "booklist": shop.BookList}

		result, err := shopCollection.UpdateOne(ctx, bson.M{"id": objId}, bson.M{"$set": update})
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			response := responses.ShopResponse{
				Status:  http.StatusInternalServerError,
				Message: "error",
				Data:    map[string]interface{}{"data": err.Error()},
			}
			json.NewEncoder(rw).Encode(response)
			return
		}

		//get updated shop details
		var updatedShop models.Shop
		if result.MatchedCount == 1 {
			err := shopCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&updatedShop)

			if err != nil {
				rw.WriteHeader(http.StatusInternalServerError)
				response := responses.ShopResponse{
					Status:  http.StatusInternalServerError,
					Message: "error",
					Data:    map[string]interface{}{"data": err.Error()},
				}
				json.NewEncoder(rw).Encode(response)
				return
			}
		}

		rw.WriteHeader(http.StatusOK)
		response := responses.ShopResponse{
			Status:  http.StatusOK,
			Message: "success",
			Data:    map[string]interface{}{"data": updatedShop},
		}
		json.NewEncoder(rw).Encode(response)
	}
}

//func GetABook() http.HandlerFunc {
//	return func(rw http.ResponseWriter, r *http.Request) {
//		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
//		params := mux.Vars(r)
//		shopId := params["shopId"]
//		bookId := params["bookId"]
//
//		var shop models.Shop
//		var book models.Book
//		defer cancel()
//
//		objId, _ := primitive.ObjectIDFromHex(shopId)
//
//		err := shopCollection.FindOne(ctx, bson.M{"id": objId}).Decode(&shop)
//		if err != nil {
//			rw.WriteHeader(http.StatusInternalServerError)
//			response := responses.ShopResponse{
//				Status:  http.StatusInternalServerError,
//				Message: "error",
//				Data:    map[string]interface{}{"data": err.Error()},
//			}
//			json.NewEncoder(rw).Encode(response)
//			return
//		}
//	}
//}
