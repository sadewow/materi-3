package models

type Book struct {
	Id     string `json:"id,omitempty" validate:"required"`
	Title  string `json:"title,omitempty" validate:"required"`
	Author string `json:"author,omitempty" validate:"required"`
}
