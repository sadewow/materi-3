package routes

import (
	"github.com/gorilla/mux"
	"go-mux-api/controllers"
)

func BookRoute(router *mux.Router) {
	router.HandleFunc("/shop/{shopId}/book", controllers.CreateBook()).Methods("POST")
}
